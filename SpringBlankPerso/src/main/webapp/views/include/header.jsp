<!DOCTYPE html>
<html lang="fr">
  <head>
  	<meta charset="utf-8">
    <title>Profil Linktogo</title>
    <link rel="icon" href="img/faviconlg.gif" type="image/gif"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.yeti.css" rel="stylesheet">
    <link href="css/docs.min.css" rel="stylesheet">
    <link href="css/style-1.b.css" rel="stylesheet">
    <meta name="author" content="Fabien Brunet - dev@fabien-brunet.fr">
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<a name="up"></a>
<div class="navbar-wrapper">
      <div class="container">
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
            	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                	<span class="sr-only">Toggle navigation</span>
                	<span class="icon-bar"></span>
                	<span class="icon-bar"></span>
                	<span class="icon-bar"></span>
              	</button>
               	<a class="navbar-brand" href=".">Fabien Brunet</a>
            </div>
            <div class="navbar-collapse collapse">
	          	<ul class="nav navbar-nav">
	          		<li id="menu_cv"><a href="cv.do">Curriculum vitae</a></li>
		            <li id="menu_dev"><a href="dev.do">D&eacute;veloppements</a></li>
		            <li id="menu_blog"><a href="#" onclick="alert('Le blog n\'est pas disponible pour le moment');">Blog</a></li>
		            <li id="menu_contact"><a href="contact.do" >Contact</a></li>
	           	</ul>
          	</div>
        </nav>
      </div>
    </div>
    <div class="container container-marging">