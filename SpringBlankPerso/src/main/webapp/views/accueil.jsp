<jsp:include page="include/header.jsp" />
<div class="row">
	<div class="col-lg-3">
		<jsp:include page="include/blocsGauche.jsp" />
	</div>
	<div class="col-lg-6">
		<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Mon parcours</h2>	
				</div>
				<div class="panel-body" style="text-align: left">
					Analyste d�veloppeur web, passionn� des nouvelles technologies, j'ai d�cid� d'�voluer dans le d�veloppement d'application J2ee et Android. Je me dirige vers la prestation de service en tant que r�f�rant technique, bas� essentiellement sur le relationnel, les contacts humains et l'expertise technique des d�veloppements Java. <br />
					<br />
					Sp�cialisations : Java, J2ee, Android, PHP, Javascript, Mysql, Oracle, Struts and Spring.				
				</div>
		</div>
		
		<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Recommandation Linkedin</h2>	
				</div>
				<div class="panel-body" style="text-align: left">
				<br />
				<table  class="col-lg-12 inputblockspaceup" >
					<tr>
						<td class="col-lg-2">
			    			JAVA
			    		</td>
			    		<td class="col-lg-10">
			    			<div class="progress">
					  			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
					  			</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-lg-2">
			    			J2EE
			    		</td>
			    		<td class="col-lg-10">
			    			<div class="progress">
					  			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
					  			</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-lg-2">
			    			CSS
			    		</td>
			    		<td class="col-lg-10">
			    			<div class="progress">
					  			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
					  			</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-lg-2">
			    			Javascript
			    		</td>
			    		<td class="col-lg-10">
			    			<div class="progress">
					  			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
					  			</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-lg-2">
			    			PHP
			    		</td>
			    		<td class="col-lg-10">
			    			<div class="progress">
					  			<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
					  			</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-lg-2">
			    			Oracle
			    		</td>
			    		<td class="col-lg-10">
			    			<div class="progress">
					  			<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
					  			</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-lg-2">
			    			Linux
			    		</td>
			    		<td class="col-lg-10">
			    			<div class="progress">
					  			<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
					  			</div>
							</div>
						</td>
					</tr>
			    </table>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<jsp:include page="include/blocsDroite.jsp" />
	</div>
</div>
<jsp:include page="include/footer.jsp" />