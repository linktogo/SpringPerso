//markread-message?id=
function majClassMarkedRead(id)
{
	$("#trMessage"+id).removeClass();
	$.ajax({
		  url: "markread-message?id="+id,
		  context: document.body
		}).done(function(msg) {
			$("#trMessage"+id).addClass("blueLu");
			document.getElementById("alertChange").innerHTML=msg;
			$("#alertChange").show();
		});
}


//notread-message?id=
function majClassNotRead(id)
{
	$("#trMessage"+id).removeClass();
	$.ajax({
		  url: "notread-message?id="+id,
		  context: document.body
		}).done(function(msg) {
			$("#trMessage"+id).addClass("warning");
			document.getElementById("alertChange").innerHTML=msg;
			$("#alertChange").show();
		});
}

//save-message?id=
function majClassArchive(id)
{
	$("#trMessage"+id).removeClass();
	$.ajax({
		  url: "save-message?id="+id,
		  context: document.body
		}).done(function(msg) {
			$("#trMessage"+id).addClass("success");
			document.getElementById("alertChange").innerHTML=msg;
			$("#alertChange").show();
		});
}

//delete-message?id=
function majClassDelete(id)
{	
	$.ajax({
		  url: "delete-message?id="+id,
		  context: document.body
		}).done(function(msg) {
			$("#trMessage"+id).hide();
		});
}


function majConversationMessageRead(id)
{
//	markread-message?id
	$("#buttonMessage"+id).removeClass();
	$("#buttonMessage"+id).addClass("btn");
	$("#buttonMessage"+id).addClass("blueLu");
	$("#aButtonMessage"+id).removeClass();
	$.ajax({
		  url: "markread-message?id="+id,
		  context: document.body
		}).done(function(msg) {
			document.getElementById("alertChange").innerHTML=msg;
			$("#alertChange").show();
		});
}


function majConversationMessageNotRead(id){
	$("#buttonMessage"+id).removeClass();
	$("#buttonMessage"+id).addClass("btn");
	$("#buttonMessage"+id).addClass("btn-primary");
	$("#aButtonMessage"+id).removeClass();
	$.ajax({
		  url: "notread-message?id="+id,
		  context: document.body
		}).done(function(msg) {
			document.getElementById("alertChange").innerHTML=msg;
			$("#alertChange").show();
		});
}

function ajaxDelete (numero){
	$.ajax({
		  url: "ajax?action=delete&numero="+numero,
		  context: document.body
		}).done(function(msg) {
//			if(msg=='ok'){
				location.reload();
//				$("#tr_list_convers_"+numero).hide();
//			}
		});
}

function majActiveIndex (strId){
	$("#"+strId).removeClass();
	$("#"+strId).addClass("active");
}

function majActiveHeader (strId){
	$("#"+strId).removeClass();
	$("#"+strId).addClass("active");
}

function submitNewMessage (){
//	alert(verifMessageMessage() && verifNumberMessage());
//	return verifMessageMessage() && verifNumberMessage();
	return verifMessageMessage();
}

function submitNewMessageProgramme (){
	return verifMessageMessage() && verifDate () && verifHour();
}

function verifMessageMessage (){
	var message =  document.getElementById('messageMessageInput').value;
	if (message.length>1) return true;
	return false;
}

function verifNumberMessage (){
	var numero =  document.getElementById('messageNumeroInput').value;
	var reg=new RegExp("[0-9]{5,15}","g");
	if(numero.match(reg)){
		return true;
	}else{
		$("#erreurMessage").show(600);
		console.log('wrong message');
	}
	return numero.match(reg);
}


function verifDate (){
	var message =  document.getElementById('date').value;
	if (message.length>1) return true;
	return false;
}

function verifHour (){
	var message =  document.getElementById('heure').value;
	if (message.length>1) return true;
	return false;
}

function checkFormulaireContact(){
	var email = document.getElementById("emailContact").value;
	var reg=new RegExp("^[a-z0-9._-]+@[a-z0-9._-]{2,}.[a-z]{2,4}$","g");
	if(!email.match(reg)){
		$("#erreurMessage").show(600);
		console.log('Email wrong');
	}
	return email.match(reg);
}

function showHidePen (showHide){
	if(showHide){
		$("#penUpdateName").show();
	}else{
		$("#penUpdateName").hide();
	}
}

function contolUpdateUser (){
	var newpasswd = document.getElementById("newpasswd").value;
	var newconfirmpasswd = document.getElementById("newconfirmpasswd").value;
	if(newpasswd==""){
		return true;
	}else{
		if(newconfirmpasswd==""){
			return false;
		}else{
			return true;
		}
	}
}

function globalSearch (){
	$("#searchButton").hide();
	$("#searchInput").show();
	
}


function searchInputWithName(){
	var searchname = document.getElementById("searchInputValue").value;
	if (searchname!=null){
		
	}
}


if (i_am_old_ie){
	$('[placeholder]').focus(function() {
		  var input = $(this);
		  if (input.val() == input.attr('placeholder')) {
		    input.val('');
		    input.removeClass('placeholder');
		  }
		}).blur(function() {
		  var input = $(this);
		  if (input.val() == '' || input.val() == input.attr('placeholder')) {
		    input.addClass('placeholder');
		    input.val(input.attr('placeholder'));
		  }
		}).blur();
}

