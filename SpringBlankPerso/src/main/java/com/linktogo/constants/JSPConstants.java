package com.linktogo.constants;


public class JSPConstants {
    public static final String HOME_JSP = "accueil";
    public static final String CV_JSP = "cv";
    public static final String DEV_JSP = "dev";
    public static final String CONTACT_JSP = "contact";
}