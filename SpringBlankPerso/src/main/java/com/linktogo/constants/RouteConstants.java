package com.linktogo.constants;
public class RouteConstants {
    public static final String HOME_ROUTE = "home.do";
    public static final String I_ROUTE = "i.do";
    public static final String CV_ROUTE = "cv.do";
    public static final String DEV_ROUTE = "dev.do";
    public static final String CONTACT_ROUTE = "contact.do";
}