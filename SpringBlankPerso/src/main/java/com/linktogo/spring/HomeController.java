package com.linktogo.spring;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.linktogo.constants.JSPConstants;
import com.linktogo.constants.RouteConstants;

@Controller
public class HomeController extends AbstractController {

	@RequestMapping(RouteConstants.HOME_ROUTE)
	public ModelAndView homeController() {
		return new ModelAndView(JSPConstants.HOME_JSP);
	}

	
	@RequestMapping(RouteConstants.I_ROUTE)
	public ModelAndView dynController() {
		return new ModelAndView(JSPConstants.HOME_JSP);
	}
	
	
	@RequestMapping(RouteConstants.DEV_ROUTE)
	public ModelAndView homeDevController() {
		return new ModelAndView(JSPConstants.DEV_JSP);
	}
	
	
	@RequestMapping(RouteConstants.CV_ROUTE)
	public ModelAndView homeCvController() {
		return new ModelAndView(JSPConstants.CV_JSP);
	}
	
	@RequestMapping(RouteConstants.CONTACT_ROUTE)
	public ModelAndView homeContactController() {
		return new ModelAndView(JSPConstants.CONTACT_JSP);
	}
	
}